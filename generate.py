#!/usr/bin/env fslpython
import json
import yaml
from collections import OrderedDict

age_range = range(36, 45)
# age_range = range(36, 39)

# header: captures top-level info relevant for the whole conceptual atlas
# (often several related data-files)
header = {}
header['provenance'] = 'Some text or something more elaborate?'
header['name'] = 'dHCP temporal atlas'
header['license'] = None
header['coordinate_system'] = None
header['citation'] = 'doi:http://dx.doi.org/10.1101/251512'
header['contact'] = 'andreas.schuh@imperial.ac.uk'
header['version'] = None
header['species'] = 'Homo Sapien'
header['description'] = 'spatio-temporal atlas from 275 healthy neonates between 35 and 44 weeks post-menstrual age (PMA)'

# resources: one per data-file/web-address/resource used to access data, each
# containing information needed to determine how to get the necessary data
# --- MODS TO HAWG ---
# type
# modality
#
#   recommended types:
#       template
#       discrete
#       prob
#   possible modalities:
#       T1w, T2w, etc.

# --------------------

res_key = ['T1w', 'T2w', 'tissue', 'structure']
res_loc = ['T1/template-{}.nii.gz',
           'T2/template-{}.nii.gz',
           'labels/tissue/label-{}.nii.gz',
           'labels/structure/label-{}.nii.gz']
res_type = ['template', 'template', 'discrete', 'discrete']

resources = {}

for rkey, rloc, rtype in zip(res_key, res_loc, res_type):

    for i in age_range:

        space_id = 'space-w{}'.format(i)
        resource_id = '_'.join(['resource-'+rkey, space_id])

        resources[resource_id] = {
            'space_id': space_id,
            'resource_name': '{}: week {}'.format(rkey, i),
            'resource_key_axis': None,
            'resource_format': 'nifti',
            'resource_location': rloc.format(i),
            'resource_nominal_resolution': None,
            'resource_type': rtype,
            'resource_modality': rkey
        }



# --- MODS TO HAWG ---
# Time
# space_id = reference to corresponding space
# value = string or numeric timepoint or range (e.g. 36, 36-44, term, adult, pubescent)
# unit = unit of time value (e.g. hours, days, weeks, months, years, ...)
# origin = origin time base (e.g. PMA, birth, experimental manipulation, 0:00:00 1970, ...)
# time_resource_index == [axis, index]
#       where axis is the axis to lookup in the resource and index is the index within that atlas
#       if resource is a 3D then axis and index == None (e.g. [None, None])
# --------------------
#
time = {}

for i in age_range:

        space_id = 'space-w{}'.format(i)
        time_id = '_'.join(['time-w{}'.format(i), space_id])

        time[time_id] = {
            'space_id': space_id,
            'time_value': i,
            'time_unit': 'weeks',
            'time_origin': 'PMA',
            'time_resource_index': [None, None]
        }



# --- SPACE ---


space = {}

for i in age_range:

        space_id = 'space-w{}'.format(i)

        space[space_id] = {
            'space_name': 'space name week {}'.format(i),
            'space_description': 'lorum ipsum grot space',
        }


# --- MODS TO HAWG ---
# mapping: space_id to space_id mapping (1->1 or 1->many)
# --------------------

mapping = {}
for i in age_range:

    space_id = 'space-w{}'

    row = {}

    for j in age_range:
        if i == j:
            continue
        row[space_id.format(j)] = 'allwarps/template-{}_to_template-{}_warp.nii.gz'.format(i, j)

    mapping[space_id.format(i)] = row


# structures: one per fundamental structure/unit stored by the atlas and contains
# only the information related to accessing that data from the resource and displaying
# it (other info in tags - see next)
# --- MODS TO HAWG ---
# resource_id = mapping to corresponding resource
# space_id = reference to corresponding space
# structure_esource_index == [axis, index]
#       where axis is the axis to lookup in the resource and index is the index within that atlas
#       if resource is a 3D then axis and index == None  (e.g. [None, None])
# structure_resource_label = numeric label for this structure in the resource index
#       None if resource is not discrete (e.g. probability map or template)
# --------------------

structures = {}

shortnames = ['CSF', 'cGM', 'WM', 'Bg', 'Ventricles', 'Cb', 'dGM', 'Bs']
longnames = [
    'Cerebrospinal Fluid',
    'Cortical Gray Matter',
    'White Matter',
    'Background',
    'Ventricles',
    'Cerebellum',
    'Deep Gray Matter',
    'Brainstem'
]

# shortnames = ['CSF', 'cGM', 'WM']
# longnames = [
#     'Cerebrospinal Fluid',
#     'Cortical Gray Matter',
#     'White Matter']

for label, name in enumerate(shortnames):

    structure_id = 'structure-' + name
    structures[structure_id] = {}

    for i in age_range:

        rkey = 'resource-tissue'
        space_id = 'space-w{}'.format(i)
        resource_id = '_'.join([rkey, space_id])

        structures[structure_id][resource_id] = {
            'structure_example_color': None,
            'structure_example_coord': None,
            'structure_resource_index': [None, None],
            'structure_resource_label': label
        }


# tags: one per scientific region/unit/meta-object of the atlas, including one for
# each fundamental structure/unit (with identical short-name/id as in 'structures').
# This is where all the scientific information (e.g. names, references,
# relationships) exists and is separate from information about access and display.

tags = {}

for label, name in enumerate(zip(shortnames, longnames)):

    tag_id = 'structure-' + name[0]

    tags[tag_id] = {
        'tag_part_of': [],
        'tag_includes': [],
        'tag_display_name': name[1],
        'tag_description': name[1],
    }

    tags[tag_id]['tag_part_of'] = ['tag_head']

tags['tag-head'] = {
        "tag_description": "The big fat head",
        "tag_display_name": "head",
        "tag_part_of": ['tag-world'],
        "tag_includes": ['structure-CSF', 'structure-cGM', 'structure-WM']
    }


tags['tag-world'] = {
        "tag_description": "The whole wide world",
        "tag_display_name": "world",
        "tag_part_of": [],
        "tag_includes": ['tag-head']
    }


# save
atlas = OrderedDict()
atlas['header'] = header
atlas['resources'] = resources
atlas['space'] = space
atlas['time'] = time
atlas['structures'] = structures
atlas['tags'] = tags
atlas['mapping'] = mapping


# This will make yaml retain the order in the ordered dict
def setup_yaml():
  """ https://stackoverflow.com/a/8661021 """
  represent_dict_order = lambda self, data:  self.represent_mapping('tag:yaml.org,2002:map', data.items())
  yaml.add_representer(OrderedDict, represent_dict_order)
setup_yaml()

with open('schuh.yml', 'w') as f:
    yaml.dump(atlas, f, default_flow_style=False)

with open('schuh.json', 'w') as f:
    json.dump(atlas, f, indent=4)


