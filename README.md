# HAWG

Extensions to MJ's HAWG proposal (2016) to account for temporal atlases.

MJ's proof-of-concept proposal is in the `MJ-propose` sub-directory (see `MJ-propose/README.md`)

`schuh.json` is a test implementation for a neonatal developmental atlas.

`generate.py` generates the json file.

The main modification is the inclusion of a `space` type, a `time` type, and `mapping` type.


- `space` defines the different spaces within the atlas.  For example, each week within the developmental atlas are considered
different spaces because they do not have voxel correspondence across the per-week templates.

- `time` defines the time increments/units within the atlas.

- `mapping` points to transforms to move between the spaces in the atlas

